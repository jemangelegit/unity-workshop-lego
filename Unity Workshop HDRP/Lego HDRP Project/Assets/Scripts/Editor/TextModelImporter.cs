﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor.Experimental.AssetImporters;
using System.IO;
using System.Linq;

[ScriptedImporter(1, "otxt")]
public class TextModelImporter : ScriptedImporter
{
    public override void OnImportAsset(AssetImportContext ctx)
    {
        var lines = File.ReadAllLines(ctx.assetPath);

        GameObject lastObject = null;
        int lastOffset = 0;

        var mainAssetIsSet = false;

        foreach (var line in lines)
        {
            var offset = line.Count(c => c == '>');
            var obj = new GameObject(line.Substring(offset));

            if (offset == 0)
            {
                ctx.AddObjectToAsset(obj.name, obj);
                if (!mainAssetIsSet) ctx.SetMainObject(obj);
            }
            else
            {
                var diff = offset - lastOffset;
                if (diff < 1)
                {
                    for (int i = 0; i >= diff; i--)
                    {
                        lastObject = lastObject.transform.parent.gameObject;
                    }
                }

                obj.transform.parent = lastObject.transform;
            }

            lastObject = obj;
            lastOffset = offset;
        }
    }
}
