﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor.Experimental.AssetImporters;
using System.IO;
using System.Linq;

[ScriptedImporter(1, "ccube")]
public class CustomCubeImporter : ScriptedImporter
{
    public override void OnImportAsset(AssetImportContext ctx)
    {
        var cube = GameObject.CreatePrimitive(PrimitiveType.Cube);

        cube.transform.position = Vector3.up;

        ctx.AddObjectToAsset(cube.name, cube);
        ctx.SetMainObject(cube);

        var rgb = File.ReadAllLines(ctx.assetPath);
        var r = (byte)int.Parse(rgb[0]);
        var g = (byte)int.Parse(rgb[1]);
        var b = (byte)int.Parse(rgb[2]);

        var material = new Material(Shader.Find("HDRP/Lit"));
        material.color = new Color32(r, g, b, 255);

        //ctx.AddObjectToAsset("My Mat", material);
    }
}
